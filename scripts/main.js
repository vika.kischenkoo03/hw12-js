// 1. Чому для роботи з input не рекомендується використовувати клавіатуру?

//Для роботи з клавіатурою призначеня події клавіатури. Тому при їх використанні в інпутах будуть недоліки.
// текст може бути вставлений мишкою, за допомогою правого кліка та меню, без жодного натискання кнопки. 
// Деякі мобільні пристрої також не генерують keypress/keydown, а одразу вставляють текст у поле.
// Обробити введення на них за допомогою клавіатурних подій не можна.
//Також не буде працювати голосове введення данних

document.addEventListener("keydown", keydown);

function keydown(event) {
  let buttons = document.querySelectorAll(".btn");
  buttons.forEach((btn) => {
    if (btn.classList.contains("blue")) {
      btn.classList.remove("blue");
    }
    if (
      `Key${btn.dataset.keybtn}` == event.code ||
      (event.key == "Enter" && btn.dataset.keybtn == "Enter")
    ) {
      btn.classList.add("blue");
    }
  });
}
